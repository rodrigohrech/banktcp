package bank.business.domain;

/**
 * @author Ingrid Nunes
 * 
 */
public class Transfer extends Transaction {

	private CurrentAccount destinationAccount;
	private StatusTransaction status;
	
	public Transfer(OperationLocation location, CurrentAccount srcAccount,
			CurrentAccount destinationAccount, double amount, StatusTransaction status) {
		super(location, srcAccount, amount);
		this.status = status;
		this.destinationAccount = destinationAccount;
	}

	/**
	 * @return the destinationAccount
	 */
	public CurrentAccount getDestinationAccount() {
		return destinationAccount;
	}

	public StatusTransaction getStatus() {
		return status;
	}

	public void setStatus(StatusTransaction status) {
		this.status = status;
	}
	
	public void allowTranfer(){
		this.status = StatusTransaction.FINALIZADA;
		
	}
}
