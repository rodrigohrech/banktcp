package bank.business.domain;

public enum StatusTransaction {
	PENDENTE, FINALIZADA, CANCELADA;
}
