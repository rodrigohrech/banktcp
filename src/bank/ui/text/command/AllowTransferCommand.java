package bank.ui.text.command;

import java.util.List;

import bank.business.AccountOperationService;
import bank.business.BusinessException;
import bank.business.domain.Transaction;
import bank.business.domain.Transfer;
//import bank.ui.TextManager;
//import bank.ui.UIAction;
import bank.ui.text.BankTextInterface;
import bank.ui.text.UIUtils;

public class AllowTransferCommand extends Command {
	
	public static final String EXIT_CODE = "E";
	
	private final AccountOperationService accountOperationService;
	
	private List<Transaction> transactionsPending;
	
	public AllowTransferCommand(BankTextInterface bankInterface,
			AccountOperationService accountOperationService) {
		super(bankInterface);
		this.accountOperationService = accountOperationService;
	}

	@Override
	public void execute() throws Exception {
		// TODO Auto-generated method stub
		
		System.out.println(getTextManager().getText("pendings"));
		
		try {
			transactionsPending = accountOperationService.getAllPendingTransactions();
			
			for (Transaction transactionCurrent : transactionsPending){ //Para cada transa��o na lista de pendentes:
				Transfer transferCurrent = (Transfer) transactionCurrent;
				System.out.println(getTextManager().getText("date")+": " +transactionCurrent.getDate().toString());
				System.out.println(getTextManager().getText("origin.account")+": " +transactionCurrent.getAccount().getId().getNumber());
				System.out.println(getTextManager().getText("destination.account")+": " +transferCurrent.getDestinationAccount().getId().getNumber());
				System.out.println(getTextManager().getText("amount")+": " +transactionCurrent.getAmount());
				
				System.out.println();
				
				UIUtils uiUtils = UIUtils.INSTANCE;	
				String commandKey = uiUtils.readString("pending.options");
				switch (commandKey){
				case "A":
					transactionCurrent.getAccount().allowTransfer(transferCurrent);
					break;
				case "C":
					transactionCurrent.getAccount().cancelTransfer(transferCurrent);
					break;
				case "N":
					break;
				case "E":
					return;
				}
				
			}
		}
		
		catch (BusinessException be){
			System.out.println(getTextManager().getText(be.getMessage()));
		}
	}

}
