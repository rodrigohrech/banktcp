package bank.ui.graphic.action;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
//import javax.swing.ButtonGroup;
import javax.swing.JButton;
//import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.AbstractTableModel;

import bank.business.AccountOperationService;
import bank.business.BusinessException;
//import bank.business.domain.CurrentAccountId;
import bank.business.domain.Transaction;
import bank.business.domain.Transfer;
import bank.ui.TextManager;
import bank.ui.graphic.BankGraphicInterface;
import bank.ui.graphic.GUIUtils;

public class AllowTransferAction extends AccountAbstractAction {

//	private class MonthYear {
//		int month;
//		int year;
//
//		@Override
//		public String toString() {
//			return textManager.getText("month." + month) + "/" + year;
//		}
//	}

	

	/**
	 * 
	 */
	private static final long serialVersionUID = 5090183202921964451L;

	private class TransactionTableModel extends AbstractTableModel {

		private static final long serialVersionUID = 2497950520925208080L;

//		private CurrentAccountId id;
		private List<Transaction> transactions;

		public TransactionTableModel(List<Transaction> transactions) {
			this.transactions = new ArrayList<>(transactions);
		}

		@Override
		public int getColumnCount() {
			return 5;
		}

		@Override
		public String getColumnName(int column) {
			String key = null;
			switch (column) {
			case 0:
				key = "date";
				break;
			case 1:
				key = "origin.account";
				break;
			case 2:
				key = "destination.account";
				break;
			case 3:
				key = "amount";
				break;
			case 4:
				key = "status";
				break;
			default:
				assert false;
				break;
			}
			return textManager.getText(key);
		}

		@Override
		public int getRowCount() {
			return transactions.size();
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			Transaction transactionCurrent = transactions.get(rowIndex);
			Object val = null;
			Transfer transferCurrent = (Transfer) transactionCurrent;
			switch (columnIndex) {
			case 0:
				val = GUIUtils.DATE_TIME_FORMAT.format(transactionCurrent.getDate());
				break;
			case 1:
				val = transactionCurrent.getAccount().getId().getNumber();
				break;
			case 2:
				
				val = transferCurrent.getDestinationAccount().getId().getNumber();
				break;
			case 3:
				val = transactionCurrent.getAmount();
				break;
			case 4: 
				val = transferCurrent.getStatus().name();
				break;

			default:
				assert false;
				break;
			}
			return val;
		}

	}


	private JPanel cards;
	private JDialog dialog;
//	private JComboBox<MonthYear> month;
	private JTable transactions;
	private List<Transaction> pendingTransaction;

	public AllowTransferAction(BankGraphicInterface bankInterface,
			TextManager textManager,
			AccountOperationService accountOperationService) {
		super(bankInterface, textManager, accountOperationService);

		super.putValue(Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		super.putValue(Action.NAME, textManager.getText("action.allow.transfer"));
	}

	public void close() {
		dialog.dispose();
		dialog = null;
	}



	@Override
	public void execute() {
		JPanel accountPanel = new JPanel(new GridLayout(2, 2, 5, 5));
		//initAndAddAccountFields(accountPanel);
//		List<Transaction> pending = new ArrayList<Transaction>();
		// Cards
		JPanel radioBtPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		this.cards = new JPanel(new CardLayout());
//		ButtonGroup btGroup = new ButtonGroup();
		


		JPanel cardsPanel = new JPanel();
		cardsPanel.setLayout(new BoxLayout(cardsPanel, BoxLayout.PAGE_AXIS));
		cardsPanel.add(accountPanel);
		cardsPanel.add(radioBtPanel);
		cardsPanel.add(cards);

		// Confirmation Buttons
		JPanel buttonsPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JButton closeButton = new JButton(textManager.getText("button.close"));
		closeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				close();
			}
		});
		buttonsPanel.add(closeButton);
		JButton allowButton = new JButton(textManager.getText("button.allow"));
		allowButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					if(pendingTransaction == null || pendingTransaction.isEmpty())
						throw new BusinessException("exception.empty.pending.transfer");
					if(transactions.getSelectedRow()==JPanel.UNDEFINED_CONDITION)
						throw new BusinessException("exception.transfer.undefined");
					
					Transfer selectedTranfer = (Transfer) pendingTransaction.get(transactions.getSelectedRow());
									
					selectedTranfer.getAccount().allowTransfer(selectedTranfer);
					transactions.updateUI();
				} catch (BusinessException be) {
					GUIUtils.INSTANCE.showMessage(bankInterface.getFrame(),
							be.getMessage(), be.getArgs(), JOptionPane.WARNING_MESSAGE);
					log.warn(be);
				} catch (Exception exc) {
					GUIUtils.INSTANCE.handleUnexceptedError(bankInterface.getFrame(),
							exc);
				}
				
			}
		});
		buttonsPanel.add(allowButton);
		
		JButton cancelButton = new JButton(textManager.getText("button.cancel"));
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					if(pendingTransaction == null || pendingTransaction.isEmpty())
						throw new BusinessException("exception.empty.pending.transfer");
					if(transactions.getSelectedRow()==JPanel.UNDEFINED_CONDITION)
						throw new BusinessException("exception.transfer.undefined");
					System.out.println(transactions.getSelectedRow());
					Transfer selectedTranfer = (Transfer) pendingTransaction.get(transactions.getSelectedRow());
									
					selectedTranfer.getAccount().cancelTransfer(selectedTranfer);
					transactions.updateUI();
				} catch (BusinessException be) {
					GUIUtils.INSTANCE.showMessage(bankInterface.getFrame(),
							be.getMessage(), be.getArgs(), JOptionPane.WARNING_MESSAGE);
					log.warn(be);
				} catch (Exception exc) {
					GUIUtils.INSTANCE.handleUnexceptedError(bankInterface.getFrame(),
							exc);
				}
				
			}
		});
		buttonsPanel.add(cancelButton);

		// Statement result
		JPanel transactionsPanel = new JPanel();
		transactionsPanel
				.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		transactions = new JTable();
		JScrollPane scrollPane = new JScrollPane(transactions,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		transactionsPanel.add(scrollPane);

		JPanel mainPanel = new JPanel(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		
		mainPanel.add(cardsPanel, BorderLayout.CENTER);


		JPanel pane = new JPanel(new BorderLayout());
		pane.add(mainPanel, BorderLayout.NORTH);
		pane.add(transactionsPanel, BorderLayout.CENTER);
		pane.add(buttonsPanel, BorderLayout.SOUTH);
		showPendentTransactions();
		
		this.dialog = GUIUtils.INSTANCE.createDialog(bankInterface.getFrame(),
				"action.allow.transfer", pane);
		this.dialog.setVisible(true);
	}

	private List<Transaction> showPendentTransactions() {

		try {
			pendingTransaction = accountOperationService.getAllPendingTransactions();
			if(pendingTransaction.isEmpty())
				throw new BusinessException("exception.empty.pending.transfer");
			
			this.transactions.setModel(new TransactionTableModel(pendingTransaction));
			
		} catch (BusinessException be) {
			GUIUtils.INSTANCE.showMessage(bankInterface.getFrame(),
					be.getMessage(), be.getArgs(), JOptionPane.WARNING_MESSAGE);
			log.warn(be);
		} catch (Exception exc) {
			GUIUtils.INSTANCE.handleUnexceptedError(bankInterface.getFrame(),
					exc);
		}
		return pendingTransaction;
	}

}
